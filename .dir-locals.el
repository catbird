((scheme-mode
  .
  ((eval . (put 'run-script 'scheme-indent-function 1))
   (eval . (put 'test-group 'scheme-indent-function 1))
   (eval . (put 'with-agenda 'scheme-indent-function 1))
   (eval . (put 'with-scene 'scheme-indent-function 1))
   (eval . (put 'with-tests 'scheme-indent-function 1)))))
