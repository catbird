;;; Catbird Game Engine
;;; Copyright © 2022 David Thompson <davet@gnu.org>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;
;; Catbird engine entry point.
;;
;;; Code:
(define-module (catbird)
  #:use-module (catbird camera)
  #:use-module (catbird config)
  #:use-module (catbird input-map)
  #:use-module (catbird kernel)
  #:use-module (catbird minibuffer)
  #:use-module (catbird mode)
  #:use-module (catbird overlay)
  #:use-module (catbird region)
  #:use-module (catbird scene)
  #:use-module (catbird ui)
  #:use-module (chickadee)
  #:use-module (chickadee graphics viewport)
  #:use-module (chickadee math rect)
  #:use-module (oop goops)
  #:export (run-catbird
            exit-catbird))

;; Add the system notification and debugging overlay.
(define (add-overlay)
  (let ((region (create-full-region #:name 'overlay #:rank 9999))
        (overlay-scene (make-overlay)))
    (set! (camera region)
          (make <camera-2d>
            #:width (rect-width (area region))
            #:height (rect-height (area region))))
    (replace-scene region overlay-scene)
    (add-minor-mode overlay-scene (make <ui-mode>))))

(define (overlay-scene)
  (scene (find-region-by-name 'overlay)))

(define-method (notify message)
  (notify (overlay-scene) message))

(define (open-minibuffer)
  (push-major-mode (overlay-scene) (make <minibuffer-mode>)))

(bind-input/global (key-press 'x '(alt)) open-minibuffer)

(define* (run-catbird thunk #:key (width 1366) (height 768)
                      (title "^~Catbird~^") (fullscreen? #f)
                      (resizable? #t) (update-hz 60)
                      (clear-color %default-clear-color))
  (let ((kernel (make <kernel>)))
    (parameterize ((current-kernel kernel))
      (run-game #:window-title title
                #:window-width width
                #:window-height height
                #:window-fullscreen? fullscreen?
                #:window-resizable? resizable?
                #:update-hz update-hz
                #:clear-color clear-color
                #:load
                (lambda ()
                  (load* kernel)
                  (thunk)
                  (add-overlay))
                #:quit exit-catbird
                #:error
                (lambda (e stack)
                  (on-error kernel e stack))
                #:draw
                (lambda (alpha)
                  (render kernel alpha))
                #:update
                (lambda (dt)
                  (update kernel dt))
                #:key-press
                (lambda (key modifiers repeat?)
                  (on-key-press kernel key modifiers))
                #:key-release
                (lambda (key modifiers)
                  (on-key-release kernel key modifiers))
                #:text-input
                (lambda (text)
                  (on-text-input kernel text))
                #:mouse-press
                ;; TODO: Handle click counter?
                (lambda (button clicks x y)
                  (on-mouse-press kernel button x y))
                #:mouse-release
                (lambda (button x y)
                  (on-mouse-release kernel button x y))
                #:mouse-move
                (lambda (x y x-rel y-rel buttons)
                  (on-mouse-move kernel x y x-rel y-rel buttons))
                #:mouse-wheel
                (lambda (x y)
                  (on-mouse-wheel kernel x y))
                #:controller-add
                (lambda (controller)
                  (on-controller-add kernel controller))
                #:controller-remove
                (lambda (controller)
                  (on-controller-remove kernel controller))
                #:controller-press
                (lambda (controller button)
                  (on-controller-press kernel controller button))
                #:controller-release
                (lambda (controller button)
                  (on-controller-release kernel controller button))
                #:controller-move
                (lambda (controller axis value)
                  (on-controller-move kernel controller axis value))
                #:window-resize
                (lambda (width height)
                  (on-window-resize kernel width height))))))

(define (exit-catbird)
  "Stop the Catbird engine."
  (abort-game))
