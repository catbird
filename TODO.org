* TODO
** TODO [#A] bug: redefining a node-2d class causes 'origin' method error
** TODO [#A] bug: redefining a mode class drops all input bindings
** TODO [#B] Add delegates to <particles> for adding/removing emitters
** TODO [#B] Add auto-resize method
It would be handy for things like =<canvas>= nodes to just ask it to
resize based on its own internal default sizing strategy.
** TODO [#B] optionally clip rendering to node's bounding box
This will require writing to the stencil buffer.
** TODO [#B] more GUI widgets
text inputs, checkboxes, windows, etc.
** TODO [#C] global minor modes
** TODO [#C] node-3d
** DONE [#A] basic graphical widgets
Containers and buttons to start with.
** DONE [#A] Bug: Passing #:width #:height to (make <node-2d>) causes unbound slot error
Example message: Slot `local-bounding-box' is unbound in object #<node-2d name: #f>
** DONE [#A] #:children init arg for <node>
** DONE [#A] observer slots
** DONE [#A] cached slots
** DONE [#A] reloadable assets
** DONE [#B] 2d render culling
** DONE [#A] FPS display
** DONE [#A] In-engine REPL
