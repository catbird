;;; Catbird Game Engine
;;; Copyright © 2023 David Thompson <davet@gnu.org>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.
(define-module (tests math)
  #:use-module (catbird math)
  #:use-module (chickadee math vector)
  #:use-module (srfi srfi-64)
  #:use-module (tests utils))

(with-tests "math"
  (test-group "vec"
    (test-equal "with 2 numbers"
      (vec2 1.0 2.0)
      (vec 1.0 2.0))
    (test-equal "with 3 numbers"
      (vec3 1.0 2.0 3.0)
      (vec 1.0 2.0 3.0))
    (test-equal "with a number and a vec2"
      (vec3 1.0 2.0 3.0)
      (vec 1.0 (vec 2.0 3.0)))
    (test-equal "with a vec2 and a number"
      (vec3 1.0 2.0 3.0)
      (vec (vec 1.0 2.0) 3.0)))
  (test-group "+"
    (test-equal "with 2 vec2s"
      (vec2 4.0 6.0)
      (+ (vec 1.0 2.0) (vec 3.0 4.0)))
    (test-equal "with a vec2 and a number"
      (vec2 4.0 5.0)
      (+ (vec 1.0 2.0) 3.0))
    (test-equal "with a number and a vec2"
      (vec2 4.0 5.0)
      (+ 3.0 (vec 1.0 2.0)))
    (test-equal "with 2 vec3s"
      (vec3 5.0 7.0 9.0)
      (+ (vec 1.0 2.0 3.0) (vec 4.0 5.0 6.0)))
    (test-equal "with a vec3 and a number"
      (vec3 5.0 6.0 7.0)
      (+ (vec 1.0 2.0 3.0) 4.0))
    (test-equal "with a number and a vec3"
      (vec3 5.0 6.0 7.0)
      (+ 4.0 (vec 1.0 2.0 3.0))))
  (test-group "-"
    (test-equal "with 2 vec2s"
      (vec2 2.0 1.0)
      (- (vec 3.0 2.0) (vec 1.0 1.0)))
    (test-equal "with a vec2 and a number"
      (vec2 2.0 1.0)
      (- (vec 3.0 2.0) 1.0))
    (test-equal "with 2 vec3s"
      (vec3 3.0 2.0 1.0)
      (- (vec 4.0 3.0 2.0) (vec 1.0 1.0 1.0)))
    (test-equal "with a vec3 and a number"
      (vec3 3.0 2.0 1.0)
      (- (vec 4.0 3.0 2.0) 1.0)))
  (test-group "*"
    (test-equal "with 2 vec2s"
      (vec2 3.0 8.0)
      (* (vec 1.0 2.0) (vec 3.0 4.0)))
    (test-equal "with a vec2 and a number"
      (vec2 3.0 6.0)
      (* (vec 1.0 2.0) 3.0))
    (test-equal "with a number and a vec2"
      (vec2 3.0 6.0)
      (* 3.0 (vec 1.0 2.0)))
    (test-equal "with 2 vec3s"
      (vec3 4.0 10.0 18.0)
      (* (vec 1.0 2.0 3.0) (vec 4.0 5.0 6.0)))
    (test-equal "with a vec3 and a number"
      (vec3 4.0 8.0 12.0)
      (* (vec 1.0 2.0 3.0) 4.0))
    (test-equal "with a number and a vec3"
      (vec3 4.0 8.0 12.0)
      (* 4.0 (vec 1.0 2.0 3.0))))
  (test-group "/"
    (test-equal "with 2 vec2s"
      (vec2 2.0 3.0)
      (/ (vec 4.0 9.0) (vec 2.0 3.0)))
    (test-equal "with a vec2 and a number"
      (vec2 2.0 4.0)
      (/ (vec 4.0 8.0) 2.0))
    (test-equal "with 2 vec3s"
      (vec3 2.0 3.0 4.0)
      (/ (vec 4.0 9.0 16.0) (vec 2.0 3.0 4.0)))
    (test-equal "with a vec3 and a number"
      (vec3 2.0 4.0 6.0)
      (/ (vec 4.0 8.0 12.0) 2.0))))
