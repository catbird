;;; Catbird Game Engine
;;; Copyright © 2022 David Thompson <davet@gnu.org>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;
;; Views into a scene.
;;
;;; Code:
(define-module (catbird camera)
  #:use-module (catbird config)
  #:use-module (catbird mixins)
  #:use-module (chickadee math)
  #:use-module (chickadee math matrix)
  #:use-module (chickadee math rect)
  #:use-module (chickadee math vector)
  #:use-module (oop goops)
  #:export (<camera>
            projection-matrix
            view-matrix
            current-camera

            <camera-2d>
            view-bounding-box
            move-to
            move-by

            <camera-3d>
            field-of-vision
            near-clip
            far-clip
            direction
            up)
  #:re-export (width height))

(define-root-class <camera> ()
  (width #:accessor width #:init-keyword #:width #:init-value 0.0)
  (height #:accessor height #:init-keyword #:height #:init-value 0.0)
  (projection-matrix #:getter projection-matrix #:init-thunk make-identity-matrix4)
  (view-matrix #:getter view-matrix #:init-thunk make-identity-matrix4))

(define-generic refresh-projection)
(define-generic refresh-view)

(define-method (initialize (camera <camera>) args)
  (next-method)
  (refresh-projection camera)
  (refresh-view camera))

(define-method (resize (camera <camera>) w h)
  (set! (width camera) w)
  (set! (height camera) h)
  (refresh-projection camera))

(define current-camera (make-parameter #f))


;;;
;;; 2D Camera
;;;

(define-class <camera-2d> (<camera> <movable-2d>)
  (view-bounding-box #:accessor view-bounding-box #:init-thunk make-null-rect))

(define-method (initialize (camera <camera-2d>) initargs)
  (next-method)
  (refresh-bounding-box-size camera))

(define-method (resize (camera <camera-2d>) w h)
  (next-method)
  (refresh-bounding-box-size camera))

(define-method (refresh-bounding-box-size (camera <camera-2d>))
  (let ((bb (view-bounding-box camera)))
    (set-rect-width! bb (width camera))
    (set-rect-height! bb (height camera))))

(define-method (refresh-projection (camera <camera-2d>))
  (orthographic-projection! (projection-matrix camera)
                            0.0 (width camera)
                            (height camera) 0.0
                            0.0 1.0))

(define-method (refresh-view (camera <camera-2d>))
  (let ((p (position camera))
        (bb (view-bounding-box camera)))
    (matrix4-translate! (view-matrix camera) p)
    (set-rect-x! bb (vec2-x p))
    (set-rect-y! bb (vec2-y p))))

(define-method (move-to (camera <camera-2d>) p)
  (vec2-copy! p (position camera))
  (refresh-view camera))

(define-method (move-by (camera <camera-2d>) d)
  (vec2-add! (position camera) d)
  (refresh-view camera))


;;;
;;; 3D Camera
;;;

(define-class <camera-3d> (<camera> <movable-3d>)
  (field-of-vision #:getter field-of-vision #:init-keyword #:field-of-vision
                   #:init-value (degrees->radians 60))
  (near-clip #:getter near-clip #:init-keyword #:near-clip #:init-value 0.1)
  (far-clip #:getter far-clip #:init-keyword #:far-clip #:init-value 5.0)
  (direction #:getter direction #:init-keyword #:direction
             #:init-form (vec3 0.0 0.0 -1.0))
  (up #:getter up #:init-keyword #:up
      #:init-form (vec3 0.0 1.0 0.0)))

(define-method (refresh-projection (camera <camera-3d>))
  (perspective-projection! (projection-matrix camera)
                           (field-of-vision camera)
                           (/ (width camera) (height camera))
                           (near-clip camera)
                           (far-clip camera)))

(define-method (refresh-view (camera <camera-3d>))
  (look-at! (view-matrix camera)
            (position camera)
            (direction camera)
            (up camera)))

(define-method (move-to (camera <camera-3d>) p)
  (vec3-copy! p (position camera))
  (refresh-view camera))

(define-method (move-by (camera <camera-3d>) d)
  (vec3-add! (position camera) d)
  (refresh-view camera))
