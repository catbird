;;; Catbird Game Engine
;;; Copyright © 2023 David Thompson <davet@gnu.org>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;
;; Generic math methods.
;;
;;; Code:
(define-module (catbird math)
  #:use-module (chickadee math matrix)
  #:use-module (chickadee math vector)
  #:use-module (oop goops)
  #:export (<vec2>
            <vec3>
            <matrix3>
            <matrix4>
            vec))

(define <vec2> (class-of (vec2 0.0 0.0)))
(define <vec3> (class-of (vec3 0.0 0.0 0.0)))
(define <matrix3> (class-of (make-identity-matrix3)))
(define <matrix4> (class-of (make-identity-matrix4)))

(define-method (vec (x <number>) (y <number>))
  (vec2 x y))

(define-method (vec (x <number>) (y <number>) (z <number>))
  (vec3 x y z))

(define-method (vec (v <vec2>) (z <number>))
  (vec3 (vec2-x v) (vec2-y v) z))

(define-method (vec (x <number>) (v <vec2>))
  (vec3 x (vec2-x v) (vec2-y v)))

(define-method (+ (a <vec2>) (b <vec2>))
  (vec2+ a b))

(define-method (+ (a <vec2>) (b <number>))
  (vec2+ a b))

(define-method (+ (a <number>) (b <vec2>))
  (vec2+ b a))

(define-method (+ (a <vec3>) (b <vec3>))
  (vec3+ a b))

(define-method (+ (a <vec3>) (b <number>))
  (vec3+ a b))

(define-method (+ (a <number>) (b <vec3>))
  (vec3+ b a))

(define-method (- (a <vec2>) (b <vec2>))
  (vec2- a b))

(define-method (- (a <vec2>) (b <number>))
  (vec2- a b))

(define-method (- (a <vec3>) (b <vec3>))
  (vec3- a b))

(define-method (- (a <vec3>) (b <number>))
  (vec3- a b))

(define-method (* (a <vec2>) (b <vec2>))
  (vec2* a b))

(define-method (* (a <vec2>) (b <number>))
  (vec2* a b))

(define-method (* (a <number>) (b <vec2>))
  (vec2* b a))

(define-method (* (a <vec3>) (b <vec3>))
  (vec3* a b))

(define-method (* (a <vec3>) (b <number>))
  (vec3* a b))

(define-method (* (a <number>) (b <vec3>))
  (vec3* b a))

(define-method (* (a <matrix3>) (b <matrix3>))
  (matrix3* a b))

(define-method (* (a <matrix4>) (b <matrix4>))
  (matrix4* a b))

(define-method (/ (a <vec2>) (b <vec2>))
  (vec2* a (vec2 (/ 1.0 (vec2-x b))
                 (/ 1.0 (vec2-y b)))))

(define-method (/ (a <vec2>) (b <number>))
  (vec2* a (/ 1.0 b)))

(define-method (/ (a <number>) (b <vec2>))
  (vec2* (vec2 (/ 1.0 (vec2-x b))
               (/ 1.0 (vec2-y b)))
         b))

(define-method (/ (a <vec3>) (b <vec3>))
  (vec3* a (vec3 (/ 1.0 (vec3-x b))
                 (/ 1.0 (vec3-y b))
                 (/ 1.0 (vec3-z b)))))

(define-method (/ (a <vec3>) (b <number>))
  (vec3* a (/ 1.0 b)))

(define-method (/ (a <number>) (b <vec3>))
  (vec3* (vec3 (/ 1.0 (vec3-x b))
               (/ 1.0 (vec3-y b))
               (/ 1.0 (vec3-z b)))
         b))
