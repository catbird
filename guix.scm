;;; Catbird Game Engine
;;; Copyright © 2022 David Thompson <davet@gnu.org>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;
;; Development environment for GNU Guix.
;;
;; To setup a development environment and build, run:
;;
;;    guix shell
;;    ./bootstrap
;;    ./configure
;;    make -j${nproc}
;;
;; To build the development snapshot, run:
;;
;;    guix build -f guix.scm
;;
;; To install the development snapshot, run:
;;
;;    guix install -f guix.scm
;;
;;; Code:
(use-modules (ice-9 match)
             (srfi srfi-1)
             (guix build-system gnu)
             (guix download)
             (guix gexp)
             (guix git-download)
             ((guix licenses) #:prefix license:)
             (guix packages)
             (guix utils)
             (gnu packages)
             (gnu packages audio)
             (gnu packages autotools)
             (gnu packages fontutils)
             (gnu packages gl)
             (gnu packages guile)
             (gnu packages image)
             (gnu packages maths)
             (gnu packages mp3)
             (gnu packages pkg-config)
             (gnu packages readline)
             (gnu packages sdl)
             (gnu packages texinfo)
             (gnu packages xiph))

(define target-guile guile-3.0-latest)

(define guile-sdl2
  (let ((commit "e9a7f5e748719ce5b6ccd08ff91861b578034ea6"))
    (package
      (name "guile-sdl2")
      (version (string-append "0.7.0-1." (string-take commit 7)))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://git.dthompson.us/guile-sdl2.git")
                      (commit commit)))
                (sha256
                 (base32
                  "0ay7mcar8zs0j5rihwlzi0l46vgg9i93piip4v8a3dzwjx3myr7v"))))
      (build-system gnu-build-system)
      (arguments
       '(#:make-flags '("GUILE_AUTO_COMPILE=0")))
      (native-inputs (list autoconf automake pkg-config texinfo))
      (inputs (list target-guile sdl2))
      (synopsis "Guile bindings for SDL2")
      (description "Guile-sdl2 provides pure Guile Scheme bindings to the
SDL2 C shared library via the foreign function interface.")
      (home-page "https://git.dthompson.us/guile-sdl2.git")
      (license license:lgpl3+))))

(define chickadee
  (let ((commit "1f1a05afca95ecd7b021a41feabecf4c525fa9ac"))
    (package
     (name "chickadee")
     (version (string-append "0.9.0-1." (string-take commit 7)))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.dthompson.us/chickadee.git")
                    (commit commit)))
              (sha256
               (base32
                "0dlcfkdhvzipn3bhjr44b2zy0yb6gn34bvz8l0cc7f9d1pprwlzl"))))
     (build-system gnu-build-system)
     (arguments
      '(#:make-flags '("GUILE_AUTO_COMPILE=0")))
     (native-inputs (list autoconf automake pkg-config texinfo))
     (inputs (list freetype
                   libjpeg-turbo
                   libpng
                   libvorbis
                   mpg123
                   openal
                   readline
                   target-guile))
     (propagated-inputs (list guile3.0-opengl guile-sdl2))
     (synopsis "Game development toolkit for Guile Scheme")
     (description "Chickadee is a game development toolkit for Guile
Scheme.  It contains all of the basic components needed to develop
2D/3D video games.")
     (home-page "https://dthompson.us/projects/chickadee.html")
     (license license:gpl3+))))

(define %source-dir (dirname (current-filename)))

(package
  (name "catbird")
  (version "0.1.0-git")
  (source (local-file %source-dir
                      #:recursive? #t
                      #:select? (git-predicate %source-dir)))
  (build-system gnu-build-system)
  (arguments
   '(#:make-flags '("GUILE_AUTO_COMPILE=0")))
  (native-inputs (list autoconf automake pkg-config texinfo))
  (inputs (list target-guile))
  (propagated-inputs (list chickadee guile-sdl2))
  (synopsis "Game engine for Scheme programmers")
  (description "Catbird is a game engine written in Guile Scheme.")
  (home-page "https://dthompson.us/projects/chickadee.html")
  (license license:gpl3+))
